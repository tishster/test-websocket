//
//  AppDelegate.h
//  Test-Websocket
//
//  Created by s on 5/9/19.
//  Copyright © 2019 s. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


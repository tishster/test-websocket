//
//  ViewController.m
//  Test-Websocket
//
//  Created by s on 5/9/19.
//  Copyright © 2019 s. All rights reserved.
//

#import "ViewController.h"
#import "SocketRocket/SocketRocket.h"

@interface ViewController ()

@end

@implementation ViewController {
    SRWebSocket *webSocket;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self connectWebSocket];
}

- (void)connectWebSocket {
    webSocket.delegate = nil;
    webSocket = nil;

    // SEND TO Server
    NSString *urlString = @"ws://su2.io/ws";
    //NSString *urlString = @"wss://echo.websocket.org";
    SRWebSocket *newWebSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:urlString]];
    newWebSocket.delegate = self;
    
    [newWebSocket open];
    //[newWebSocket send:@"abcuser"];
}

- (void)webSocketDidOpen:(SRWebSocket *)newWebSocket {
    webSocket = newWebSocket;
    //[webSocket send:[NSString stringWithFormat:@"Hello from %@", [UIDevice currentDevice].name]];
    
#if 0
    NSArray *myArray = [[NSArray alloc] initWithObjects:@"author",@"user_id_xx",@"group_id",@"group_xx", nil];
;
    NSError *error=nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:myArray options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
#endif
#if 0
    NSError *error;
    NSDictionary *dictionaryOrArrayToOutput = [[NSDictionary alloc] initWithObjectsAndKeys: @"author",@"user_id_xx",@"group_id",@"group_xx", nil];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryOrArrayToOutput
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        printf("%s", [jsonString UTF8String]);
        [webSocket send:jsonString];
    }
#endif
    NSError *error;
    
    //NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys: @"user_1",@"author",@"group_10",@"group_id", nil];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithObjectsAndKeys: @"user_1",@"author", nil];
    NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithObjectsAndKeys: @"chat", @"type", nil];
    [json setObject:data forKey:@"data"];
                                 
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
        options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                                      error:&error];
    if (! jsonData) {
    NSLog(@"Got an error: %@", error);
                                 } else {
                                     NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                     printf("%s", [jsonString UTF8String]);
                                     [webSocket send:jsonString];
                                     
                                     [webSocket send: @"USER 1 xx"];
                                 }

    //[webSocket send:@"test1 for group 10"];
   
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    //self.messagesTextView.text = [NSString stringWithFormat:@"%@\n%@", self.messagesTextView.text, message];
    
    printf("Received from Server: %s", [message UTF8String]);
}

@end
